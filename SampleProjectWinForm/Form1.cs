﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SampleProjectWinForm
{
    public partial class Form1 : Form
    {
        //SQLConnection nos permite manejar el acceso al servidor
        SqlConnection conn;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            //serverName DESKTOP-I7HFCV1
            String servidor = txtServidor.Text;
            String bd = txtBaseDatos.Text;
            String user = txtUsuario.Text;
            String pwd = txtPassword.Text;

            if (servidor != "" && bd != "")
            {
                //Definir cadena de conexion
                String str = $"Server={servidor};DataBase={bd};";
                //La cadena de conexion cambia en funcion al checkbox
                if (chkAutenticacion.Checked)
                    str += "Integrated Security=true";
                else
                    str += $"User Id={user};Password={pwd};";
                //Abrir conexion con el servidor por medio de la cadena
                try
                {
                    conn = new SqlConnection(str);
                    conn.Open();
                    btnDesconectar.Enabled = true;
                    btnEstado.Enabled = true;
                    
                    frmLogin login = new frmLogin(conn);
                    login.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al conectar al servidor:\n" + ex.ToString());
                }
            } else
            {
                MessageBox.Show("Especifique la conexión para continuar.");
            }
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificaremos que la conexion no este cerrada
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                    MessageBox.Show("Conexión cerrada satisfactoriamente");
                    btnDesconectar.Enabled = false;
                    btnEstado.Enabled = false;
                }
                else
                    MessageBox.Show("La conexión ya esta cerrada");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cerrar la conexión:\n" +
                    ex.ToString());
            }
        }

        private void btnEstado_Click(object sender, EventArgs e)
        {
            //Por medio de esta funcion se intentara obtener el estado de la conexion
            try
            {
                //Si la conexion esta abierta recuperamos su informacion
                if (conn.State == ConnectionState.Open)
                    MessageBox.Show("Estado del servidor: " + conn.State +
                        "\nVersión del servidor: " + conn.ServerVersion +
                        "\nBase de datos: " + conn.Database);
                else
                    MessageBox.Show("Estado del servidor: " + conn.State);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Imposible determinar el estado del servidor:\n" +
                    ex.ToString());
            }
        }

        private void chkAutenticacion_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAutenticacion.Checked)
            {
                txtUsuario.Enabled = false;
                txtUsuario.BackColor = Color.Gray;
                txtPassword.Enabled = false;
                txtPassword.BackColor = Color.Gray;
            }
            else
            {
                txtUsuario.Enabled = true;
                txtUsuario.BackColor = Color.FromArgb(64,64,64);
                txtPassword.Enabled = true;
                txtPassword.BackColor = Color.FromArgb(64, 64, 64);
            }
        }
    }
}
