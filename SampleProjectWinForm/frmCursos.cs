﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SampleProjectWinForm
{
    public partial class frmCursos : Form
    {
        SqlConnection conn;
        public frmCursos(SqlConnection conn)
        {
            InitializeComponent();
            this.conn = conn;
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    String sql = "SELECT * FROM Course";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    DataTable table = new DataTable();
                    table.Load(reader);
                    dgvListado.DataSource = table;
                    dgvListado.Refresh();
                }
                else
                {
                    MessageBox.Show("La conexión está cerrada");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error inesperado:\n", ex.ToString());
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                String title = txtCurso.Text;

                if (conn.State == ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "BuscarCurso";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@courseTitle";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = title;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListado.DataSource = dt;
                    dgvListado.Refresh();
                }
                else
                {
                    MessageBox.Show("La conexión está cerrada");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error inesperado:\n", ex.ToString());
            }
        }
    }
}
