﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace SampleProjectWinForm
{
    public partial class frmLogin : Form
    {
        SqlConnection conn;
        public frmLogin(SqlConnection conn)
        {
            this.conn = conn;
            InitializeComponent();
        }
        //Esto permitirá mover el formulario :D
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //Funcion para ingresar al sistema
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            String userText = txtUsuario.Text.Trim();
            String passText = txtPassword.Text.Trim();

            if (userText != "" && passText != "")
            {
                //un try catch en caso de algun error inesperado
                try
                {
                    //verificar si la conexion esta abierta
                    if (conn.State == ConnectionState.Open)
                    {
                        //comenzamos la consulta
                        SqlCommand cmdLogin = new SqlCommand();
                        cmdLogin.CommandText = "LoginOfUser";
                        cmdLogin.CommandType = CommandType.StoredProcedure;
                        cmdLogin.Connection = conn;

                        SqlParameter paramUser = new SqlParameter();
                        paramUser.ParameterName = "@username";
                        paramUser.SqlDbType = SqlDbType.NVarChar;
                        paramUser.Value = userText;

                        SqlParameter paramPassword = new SqlParameter();
                        paramPassword.ParameterName = "@password";
                        paramPassword.SqlDbType = SqlDbType.NVarChar;
                        paramPassword.Value = passText;

                        cmdLogin.Parameters.Add(paramUser);
                        cmdLogin.Parameters.Add(paramPassword);

                        if (cmdLogin.ExecuteScalar() != null)
                        {
                            frmPrincipalMDI principalMDI = new frmPrincipalMDI(conn);
                            principalMDI.Show();
                            principalMDI.FormClosed += Logout;
                            Hide();
                            lblError.Text = "";
                        }
                        else
                        {
                            lblError.Text = "Credenciales incorrectas";
                            txtPassword.Clear();
                            txtPassword.Focus();
                        }
                    }
                    else
                        MessageBox.Show("La conexión con la base de datos ha sido cerrada.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
                }
            }
            else
            {
                lblError.Text = "Rellene sus datos por favor";
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            conn.Close();
            Application.Exit();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            txtUsuario.Focus();
            lblError.Text = "";
        }

        private void linkTwitter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Specify that the link was visited.
            linkTwitter.LinkVisited = true;
            // Navigate to a URL.
            System.Diagnostics.Process.Start("https://twitter.com/frenrihr_code");
        }

        private void linkFreepik_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.freepik.es/");
        }

        private void titleBar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0x112, 0xf012, 0);
        }

        private void Logout(object sender, FormClosedEventArgs args)
        {
            txtPassword.Clear();
            txtUsuario.Clear();
            Show();
            txtUsuario.Focus();
        }
    }
}
