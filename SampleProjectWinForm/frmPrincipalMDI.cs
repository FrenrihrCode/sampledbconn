﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SampleProjectWinForm
{
    public partial class frmPrincipalMDI : Form
    {
        SqlConnection conn;
        public frmPrincipalMDI(SqlConnection conn)
        {
            InitializeComponent();
            this.conn = conn;
        }

        private void mnuSalir_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Estás seguro de cerrar la aplicación? \nSe interrumpirá cualquier proceso!", 
                "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                conn.Close();
                Application.Exit();
            }
        }

        private void mnuCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de cerrar tu sesión? \nSe interrumpirá cualquier proceso!",
                "Cerrar Sesión", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }

        private void mnuPersonas_Click(object sender, EventArgs e)
        {
            Persona persona = new Persona(conn)
            {
                MdiParent = this
            };
            persona.Show();
        }

        private void mnuCursos_Click(object sender, EventArgs e)
        {
            frmCursos persona = new frmCursos(conn)
            {
                MdiParent = this
            };
            persona.Show();
        }
    }
}
