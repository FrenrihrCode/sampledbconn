USE [SchoolProject]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 19/10/2020 16:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[usuario_id] [int] IDENTITY(1,1) NOT NULL,
	[usuario_nombre] [varchar](10) NOT NULL,
	[usuario_password] [varchar](20) NOT NULL,
	[usuario_fecha_registro] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[LoginOfUser]    Script Date: 19/10/2020 16:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LoginOfUser] 
	@username varchar(50), 
	@password varchar(50)
AS
  begin
       SELECT * FROM Usuarios 
       WHERE usuario_nombre=@username and usuario_password=@password
  end
GO
