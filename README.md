# Windows Form Sample Connection to DB
Este proyecto es una aplicación de escritorio de Windows. Permite conectarse a una base de datos SQL Server y presenta una interfaz de logueo moderno.
### Puedes clonar este repositorio y probarlo por tu cuenta

## Imágenes de muestra
![](winform1.png)
![](winform2.png)

## Importante
Este proyecto esta basado en una conexión local, es solo una muestra de como funciona Windows Form y C#.
Por lo que si vas a probarlo es necesario que **Cambies los datos de las tablas por las de tu proyecto**!
A modo de ayuda se proporciona un Script para crear un base de datos con la tabla Usuarios para el login y su respectivo procedimiento almacenado. Eres libre de usar el script y correrlo en su Servidor, sin embargo se recomeinda **altamente** que use su propia base de datos.

## Licencia
[MIT](https://choosealicense.com/licenses/mit/)